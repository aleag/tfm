# Sistema de detección y monitorización de amenazas de ciberseguridad utilizando aprendizaje profundo y la pila Elastic

#### _Cybersecurity threat detection and monitoring system using Deep Learning and the Elastic Stack_

## Contexto

En este trabajo final del máster de Ciencia de datos de la UOC se ha diseñado e implementado un sistema completo de detección de anomalías de ciberseguridad utilizando aprendizaje profundo (_deep learning_) y la pila Elastic (_Elasticsearch, Kibana, Logstash y Filebeat_, versión `7.12.0`). Esta herramienta es capaz de procesar eventos de seguridad de cualquier dispositivo con sistema operativo _Windows_ y determinar si se trata de una posible amenaza o no.

Para facilitar su instalación y uso, todo el sistema está implementando utilizando contenedores de Docker. En total, se han creado cuatro contenedores:

1. `generador-eventos`: Aplicación web para cargar los ficheros de eventos en formato xml o generar nuevos eventos de forma manual. En este contenedor se realiza el análisis de los eventos utilizando la red neuronal y también tiene instalado Filbeat para capturar la información de los ficheros de log de la aplicación.
2. `logstash`: parsear y dar el formato adecuado a los datos capturados por Filebeat.
3. `elasticserch`: almacenamiento de los datos parseados por Logstash.
4. `kibana`: visualización mediante dashboard de los datos.

## Contenido del repositorio

      .
      ├── dashboard            # Dashboard para importar en Kibana
      ├── detector-anomalias   # Código python para construir la red neuronal
      ├── docker               # Ficheros de configuración de docker
      ├── generador-eventos    # Código python para construir la aplicación web
      └── scripts              # Scripts para configurar el entorno automáticamente

## Descarga e instalación

A continuación se describen los pasos necesarios para descargar e instalar el sistema:

1. Descargar e instalar [Docker Desktop](https://www.docker.com/products/docker-desktop).
2. Asignar al menos 8Gb de memoria RAM en la configuración de Docker Desktop (engranaje de la parte superior derecha -> resources).
3. Descargar repositorio [GitLab](https://gitlab.com/aleag/tfm).
4. Crear las imágenes y contenedores de Docker. Se ha creado un script que automatiza el proceso: 
```
$ cd tfm-master/scripts
$ ./create
```
Cuando finalice la ejecución de este script, ya tendremos el sistema instalado y funcionando. Para acceder, basta con abrir un navegador web y visitar estas direcciones:
* Aplicación web: http://localhost:8080
* Kibana: http://localhost:5601

Para parar los contenedores, ejecutamos el script `stop`. Para volver a arrancar los contenedores, ejecutamos el comando `start`. Para eliminar todo, ejecutamos el comando `delete`.

Para importar el _dashboard_ en Kibana:
1. Acceder a Kibana (http://localhost:5601).
2. En el menú de opciones de la izquierda, seleccionar _Stack Management_.
3. Accedemos a la opción _Saved Objects_.
4. En la barra superior, seleccionamos _Import_ y arrastramos el fichero que se encuentra en el directorio _dashboard_ de este repositorio.