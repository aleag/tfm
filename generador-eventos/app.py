from flask import Flask
from flask import render_template, request, redirect, url_for
import json
import random
from datetime import datetime
import xml.etree.ElementTree as ET
from src.event import Event
import pandas as pd
import numpy as np
import pickle5 as pickle
from keras.models import load_model
app = Flask(__name__)

# Umbral para determinar si un evento se marca como anomalia
THRESHOLD = 1.558712

# Cargamos la red neuronal
loaded_autoencoder = load_model('./dl_models/autoencoder.h5')


@app.route('/')
def index():
    return render_template("index.html")

@app.route('/file', methods=["GET", "POST"])
def file():
    if request.method == 'POST':
        file = parse_file(request)
         
        return render_template("new_file.html")
    return render_template("file.html")

@app.route('/event', methods=["GET", "POST"])
def event():
    if request.method == 'POST':
        event = parse_request(request)
        save(analyzed_event(event))

        return render_template("new_event.html")
    return render_template("event.html")

### Métodos de utilidad ###

def parse_request(request):
    return {'eventID': request.form['eventID'],
            'timeCreated': request.form['timeCreated'],
            'processID': request.form['processID'],
            'threadID': request.form['threadID'],
            'userName': request.form['userName'],
            'domainName': request.form['domainName'],
            'callerProcessName': request.form['callerProcessName'],
            'longMessage': request.form['message'],
            'level': request.form['level'],
            'task': request.form['task']}

def load_and_tokenize_attribute(attribute, name):
    with open('./dl_models/' + name + '.pickle', 'rb') as handle:
        t = pickle.load(handle)
        
    tokenized_attribute = t.texts_to_matrix(attribute, mode='count')
    
    return pd.DataFrame(tokenized_attribute)

def isAnomaly(event):
    event_df = pd.DataFrame(event, index=[1])
    
    attr_to_tokenize = ['eventID', 'processID', 'userName', 'domainName', 
                    'callerProcessName', 'longMessage', 'level', 'task']

    processed_event = pd.DataFrame()

    # Añadimos los datos procesados
    for attr in attr_to_tokenize:
        processed_event = pd.concat([processed_event, load_and_tokenize_attribute(event_df[attr], attr)], axis=1)
        
    threadID = event_df.iloc[:, 3].values

    with open('./dl_models/threadID.pickle', 'rb') as handle:
        scaler = pickle.load(handle)

    processed_event['threadID'] = scaler.transform(threadID.reshape(-1,1))
    
    pred = loaded_autoencoder.predict(processed_event)
    anomaly_score = np.mean(np.power(processed_event - pred, 2), axis=1)
    return (anomaly_score.item() > THRESHOLD, anomaly_score.item())

def analyzed_event(event):
    anomaly = isAnomaly(event)
    event['anomaly'] = anomaly[0]
    event['anomalyScore'] = anomaly[1]
    return event

def save(user_data):
    with open("/home/events.out", mode="a", encoding="utf-8") as outfile: 
        json.dump(user_data, outfile, ensure_ascii=False)
        outfile.write('\n')

def parse_file(request):
    file = request.files['file']
    raw_xml = file.read()
    file.close()

    parsed_xml = ET.fromstring(raw_xml)

    for event in parsed_xml:
        e = Event(event)
        row = analyzed_event(e.createRow())
        save(row)