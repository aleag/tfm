class Event:
    def __init__(self, str_event):
        self.str_event = str_event
        self.namespace = 'http://schemas.microsoft.com/win/2004/08/events/event'
        
    def getEventID(self):
        return self.parseElement(
            self.str_event.find('./{' + self.namespace + '}System/{'+ self.namespace + '}EventID'))
        
    def getTimeCreated(self):
        return self.str_event[0][7].attrib['SystemTime']
    
    def getProcessID(self):
        return self.str_event[0][10].attrib['ProcessID']
    
    def getThreadID(self):
        return self.str_event[0][10].attrib['ThreadID']
        
    def getUserName(self):
        return self.parseElement(
            self.str_event.find('./{' + self.namespace + '}EventData/{'+ self.namespace + '}Data[@Name="SubjectUserName"]'))
        
    def getDomainName(self):
        return self.parseElement(
            self.str_event.find('./{' + self.namespace + '}EventData/{'+ self.namespace + '}Data[@Name="SubjectDomainName"]'))
        
    def getCallerProcessName(self):
        return self.parseElement(
            self.str_event.find('./{' + self.namespace + '}EventData/{'+ self.namespace + '}Data[@Name="CallerProcessName"]'))
        
    def getLongMessage(self):
        return self.parseElement(
            self.str_event.find('./{' + self.namespace + '}RenderingInfo/{'+ self.namespace + '}Message'))
        
    def getLevel(self):
        return self.parseElement(
            self.str_event.find('./{' + self.namespace + '}RenderingInfo/{'+ self.namespace + '}Level'))
        
    def getTask(self):
        return self.parseElement(
            self.str_event.find('./{' + self.namespace + '}RenderingInfo/{'+ self.namespace + '}Task'))
    
    def parseElement(self, element):
        return element.text if element is not None else 'Desconocido'
    
    def createRow(self):
        return {'eventID': self.getEventID(),
                'timeCreated': self.getTimeCreated(),
                'processID': self.getProcessID(),
                'threadID': self.getThreadID(),
                'userName': self.getUserName(),
                'domainName': self.getDomainName(),
                'callerProcessName': self.getCallerProcessName(),
                'longMessage': self.getLongMessage(),
                'level': self.getLevel(),
                'task': self.getTask()}